#ifndef _BMP_H_
#define _BMP_H_

#include <stdio.h>
#include <stdbool.h>
#include "../file.h"

#define COLOR_DEPTH 24
#define BMP_TYPE 0x4d42
#define SIZE_PIXEL sizeof(struct pixel)
#define SIZE_HEADER sizeof(struct bmp_header)

#define FOR_BMP_HEADER( FOR_FIELD ) \
        FOR_FIELD( uint16_t,bfType)\
        FOR_FIELD( uint32_t,bfileSize)\
        FOR_FIELD( uint32_t,bfReserved)\
        FOR_FIELD( uint32_t,bOffBits)\
        FOR_FIELD( uint32_t,biSize)\
        FOR_FIELD( uint32_t,biWidth)\
        FOR_FIELD( uint32_t,biHeight)\
        FOR_FIELD( uint16_t,biPlanes)\
        FOR_FIELD( uint16_t,biBitCount)\
        FOR_FIELD( uint32_t,biCompression)\
        FOR_FIELD( uint32_t,biSizeImage)\
        FOR_FIELD( uint32_t,biXPelsPerMeter)\
        FOR_FIELD( uint32_t,biYPelsPerMeter)\
        FOR_FIELD( uint32_t,biClrUsed)\
        FOR_FIELD( uint32_t,biClrImportant)

#define DECLARE_FIELD( t, n ) t n ;

struct __attribute__((packed)) bmp_header 
{
   FOR_BMP_HEADER( DECLARE_FIELD )
};


void bmp_header_print( struct bmp_header const* header, FILE* f );
bool read_header_from_file( const char* filename, struct bmp_header* header );
bool read_header( FILE* f, struct bmp_header* header );
struct bmp_header header_from_img (struct image const *img);
uint8_t get_padding(struct image const* img);

#endif
