#include "bmp.h"

#include <inttypes.h>
#include <stdio.h>
#include <stdbool.h>

#define PRI_SPECIFIER(e) (_Generic( (e), uint16_t : "%" PRIu16, uint32_t: "%" PRIu32, default: "NOT IMPLEMENTED" ))

#define PRINT_FIELD( t, name ) \
    fprintf( f, "%-17s: ",  # name ); \
    fprintf( f, PRI_SPECIFIER( header-> name ) , header-> name );\
    fprintf( f, "\n");


void bmp_header_print( struct bmp_header const* header, FILE* f ) {

   FOR_BMP_HEADER( PRINT_FIELD )
}

bool read_header( FILE* f, struct bmp_header* header ) {
    return fread( header, SIZE_HEADER, 1, f );
}

bool read_header_from_file( const char* filename, struct bmp_header* header ) {
    if (!filename)
      return false;
    FILE* f = fopen( filename, "rb" ); 
    if (!f)
      return false;
    if (read_header( f, header ) ) {
        fclose( f );
        return true; 
    }
    fclose( f );
    return false;
}

struct bmp_header header_from_img (struct image const *img){
    uint64_t w = img -> width, h = img -> height;
    struct bmp_header header = (struct bmp_header){
            .bfType = BMP_TYPE,
            .bfileSize = w * h * SIZE_PIXEL + SIZE_HEADER + w % 4 * h,
            .bfReserved = 0,
            .bOffBits = SIZE_HEADER,
            .biSize = 40,
            .biWidth = w,
            .biHeight = h,
            .biPlanes = 1,
            .biBitCount = COLOR_DEPTH,
            .biCompression = 0,
            .biSizeImage = w * h * SIZE_PIXEL,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0,
    };

    return header;
}

uint8_t get_padding(struct image const* img) {
    return (4 - (img -> width * SIZE_PIXEL % 4)) % 4;
}
