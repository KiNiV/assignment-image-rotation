CFLAGS=--std=c18 -Wall -pedantic -Isrc/ -ggdb -Wextra -Werror -DDEBUG
CC=gcc

all: rtt

bmp.o: image_format/bmp.c
	$(CC) -c $(CFLAGS) $< -o $@

util.o: util.c
	$(CC) -c $(CFLAGS) $< -o $@

main.o: main.c
	$(CC) -c $(CFLAGS) $< -o $@

rotation.o: actions/rotation.c
	$(CC) -c $(CFLAGS) $< -o $@

status_commands.o: status_commands.c
	$(CC) -c $(CFLAGS) $< -o $@

rtt: main.o util.o bmp.o rotation.o status_commands.o
	$(CC) -o rtt $^

clean:
	rm -f main.o util.o bmp.o rotation.o status_commands.o rtt

