//
// Created by Кирилл on 21.01.2021.
//
#include "status_commands.h"
#include <stdlib.h>


enum open_status fopen_status(const char* fname, FILE** in ){
  if (!fname)
    return OPEN_ERROR;
  *in = fopen(fname, "rb");
  if(!(*in))
    return OPEN_ERROR;
  return OPEN_OK;
}

enum read_status check_before_reading(FILE* in, struct bmp_header *header){
  if(!read_header(in, header))
    return READ_INVALID_HEADER;

  if(header -> bfType != BMP_TYPE)
    return READ_INVALID_SIGNATURE;

  if (header -> biBitCount != COLOR_DEPTH)
    return READ_UNSUPPORTED_DEPTH;

  return READ_OK;
}

enum read_status from_bmp(FILE* in, struct image* img){
    struct bmp_header header = {0};

    enum read_status status = check_before_reading(in, &header);

    if(status != READ_OK) return status;

    const size_t h = header.biHeight, w = header.biWidth;
    img -> width = w;
    img -> height = h;
    img -> data = (struct pixel*) malloc(h * w * SIZE_PIXEL);

    if(fseek(in, SIZE_HEADER, SEEK_SET))
    {
      free(img -> data);
      return READ_INVALID_BITS;
    }

    for (size_t i = 0; i < h; i++) {
        if(fread(&(img->data[i * w]), w, SIZE_PIXEL, in) < SIZE_PIXEL){
          free(img -> data);
          return READ_INVALID_BITS;
        }
        if(fseek(in, get_padding(img), SEEK_CUR)){
          free(img -> data);
          return READ_INVALID_BITS;
        }

    }

    return READ_OK;
}



enum write_status to_bmp(FILE* out, struct image const* img){
    struct bmp_header header = header_from_img(img);
    if(!fwrite(&header, 1, SIZE_HEADER, out))
        return WRITE_ERROR;

    const size_t h = img -> height, w = img -> width;

    const uint32_t padding = get_padding(img);

    char emptyCh[3] = {0};

    for (size_t i = 0; i < h; ++i) {
        if (!fwrite(&img->data[i * w], w, SIZE_PIXEL, out))
            return WRITE_ERROR;
        if (!fwrite(emptyCh, 1, padding, out) && padding!=0)
            return WRITE_ERROR;
    }

    return WRITE_OK;
}

enum close_status fclose_status(FILE** out){
    if(!fclose(*out))
        return CLOSE_OK;
    return CLOSE_ERROR;
}