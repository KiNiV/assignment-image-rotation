//
// Created by Кирилл on 20.01.2021.
//

#ifndef _ROTATION_H
#define _ROTATION_H
#include "../file.h"
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

/* создаёт копию изображения, которая повёрнута на 90 градусов */
struct image rotate( struct image const source, bool clockwise);

#endif //ROTATION_H
