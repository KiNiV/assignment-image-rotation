//
// Created by Кирилл on 20.01.2021.
//

#include "rotation.h"


struct image rotate( struct image const source, bool clockwise){
    struct image send_img;
    uint64_t w = source.height, h = source.width;
    send_img.height = h;
    send_img.width = w;
    send_img.data = (struct pixel*) malloc(source.width * source.height * sizeof(struct pixel));

    uint64_t koeffData1=0;
    uint64_t koeffData2=0;
    for (size_t x = 0; x < w; x++) {
        for (size_t y = 0; y < h; y++) {
            if(clockwise) {koeffData1 = x; koeffData2 = h-y-1;} else {koeffData1 = w-x-1; koeffData2 = y;}
            send_img.data[x+(koeffData2 * send_img.width)] = source.data[y+ koeffData1 * h];
        }
    }
    return send_img;
}