# Assignment: Image rotation
---
Лабораторная работа: Поворот картинки

# Description

This program can rotate 90 degrees your BMP picture. You can choose direction of rotation.

# Example of launching

```c
// how to launch program with linux console (make)
make all //making -o files
./rtt input.bmp //launching program itself with clockwise rotation
make clean //clear -o files
//===========================
make all // making -o files
./rtt input2.bmp false // launching program with counterclock-wise rotation
make clean // clear -o files

```
# Result
Your new image will be save as "output.bmp"

![Alt text](input.bmp?raw=true "Input")

![Alt text](output.bmp?raw=true "Output")
