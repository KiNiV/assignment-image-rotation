#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include "actions/rotation.h"
#include "image_format/bmp.h"
#include "status_commands.h"
#include "util.h"


void usage() {
    fprintf(stderr, "Follow this form: ./rotate_pls BMP_FILE_NAME (CLOCKWISE)\n\"/print_header\" - command\nCLOCKWISE - type \"true\" or \"false\"");

}

int main( int argc, char** argv ) {
    if (argc != 3 && argc != 2)
      usage();
    if (argc < 2)
      err("Not enough arguments \n" );
    if (argc > 3)
      err("Too many arguments \n" );

    bool clockwise = true;
    if(argc == 3){
        if (strcmp(argv[2],"true") == 0)
            clockwise = true;
        else if (strcmp(argv[2],"false") == 0)
            clockwise = false;
        else
            err("Wrong third argument\n");
    }


    struct bmp_header h = { 0 };
    if (read_header_from_file( argv[1], &h )) {
        bmp_header_print( &h, stdout );
    }
    else {
        err( "Failed to open BMP file or reading header.\n" );
    }

    //Открываем файл исходный файл

    FILE* f;
    enum open_status status1 = fopen_status(argv[1], &f);
    switch (status1)
      {
      case OPEN_ERROR:
        err("Error while opening.\n");
      case OPEN_OK:
        fprintf(stderr, "Opening is successful.\n");
      }
    //конвертируем bmp в image и закрываем исходный файл

    struct image img;
    enum read_status status2 = from_bmp(f, &img);
    switch (status2) {
        case READ_INVALID_SIGNATURE:
            err("Invalid signature in file.\n");
        case READ_INVALID_HEADER:
            err("Invalid header in file.\n");
        case READ_INVALID_BITS :
            err("Invalid bits if file.\n");
        case READ_UNSUPPORTED_DEPTH:
            err("Unsupported depth of color in file.\n");
        case READ_OK:
            fprintf(stderr, "Reading is successful.\n");
            break;
    }
    enum close_status status3 = fclose_status(&f);
    switch (status3) {
        case CLOSE_ERROR:
            err("Closing wasn't so good\n");
        case CLOSE_OK:
            fprintf(stderr, "First file closing is successful.\n");
    }
    //переворачиваем image
    img = rotate(img, clockwise);
    //конвертируем image в bmp, открываем новый файл, записываем картинку
    FILE* nf = fopen("output.bmp", "wb");

    enum write_status status4 = to_bmp(nf, &img);
    switch (status4) {
        case WRITE_ERROR:
            err("Something wrong with writing file.\n");
        case WRITE_OK:
            fprintf(stderr, "Writing is successful.\n");
    }
    //
    enum close_status status5 = fclose_status(&nf);
    switch (status5) {
        case CLOSE_ERROR:
            err("Closing wasn't so good\n");
        case CLOSE_OK:
            fprintf(stderr, "Second file closing is successful.\n");
    }

    fprintf(stderr, "Rotated file is successfully saved as output.bmp.\n");
    return 0;
}