//
// Created by Кирилл on 21.01.2021.
//

#ifndef STATUS_COMMANDS_H
#define STATUS_COMMANDS_H
#include "file.h"
#include "image_format/bmp.h"



enum open_status {
  OPEN_OK = 0,
  OPEN_ERROR
};

enum open_status fopen_status(const char* fn, FILE** fin );

/*  deserializer   */
enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_UNSUPPORTED_DEPTH
};

enum read_status from_bmp( FILE* in, struct image* img );

//from для других форматов

/*  serializer   */
enum  write_status  {
  WRITE_OK = 0,
  WRITE_ERROR
};

enum write_status to_bmp( FILE* out, struct image const* img );

//to для других форматов

enum close_status {
  CLOSE_OK = 0,
  CLOSE_ERROR
};

enum close_status fclose_status(FILE** out);

#endif // STATUS_COMMANDS_H
